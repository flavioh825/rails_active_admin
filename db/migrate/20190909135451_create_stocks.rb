class CreateStocks < ActiveRecord::Migration[5.0]
  def change
    create_table :stocks do |t|
      t.string :symbol
      t.decimal :purchase_price, precision: 30, scale: 2
      t.integer :quantity
      t.integer :remaining
      t.references :portfolio, foreign_key: true

      t.timestamps
    end
  end
end
