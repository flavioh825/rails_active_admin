# Marcos
marcos = User.create(name: 'Marcos Afonso dos Santos', email: 'marcos@gmail.com', username: 'marcosaf')
pm1 = Portfolio.create(title: 'Carteira do Marcos', user_id: marcos.id)
pm2 = Portfolio.create(title: 'Segunda carteira do Marcos', user_id: marcos.id)
pm3 = Portfolio.create(title: 'Terceira carteira do Marcos', user_id: marcos.id)
Stock.create(portfolio_id: pm1.id, symbol: 'PETR4', purchase_price: 120.00, quantity: 10, remaining: 10)
Stock.create(portfolio_id: pm2.id, symbol: 'ITUB4', purchase_price: 200.00, quantity: 5, remaining: 5)
Stock.create(portfolio_id: pm3.id, symbol: 'VALE3', purchase_price: 180.00, quantity: 15, remaining: 15)

# Tereza
tereza = User.create(name: 'Tereza Almeida Melo', email: 'tereza@gmail.com', username: 'terezaam')
pt1 = Portfolio.create(title: 'Carteira da Tereza', user_id: tereza.id)
pt2 = Portfolio.create(title: 'Segunda carteira da Tereza', user_id: tereza.id)
Stock.create(portfolio_id: pt1.id, symbol: 'ITSA4', purchase_price: 250.00, quantity: 5, remaining: 5)
Stock.create(portfolio_id: pt2.id, symbol: 'GOLL4', purchase_price: 60.00, quantity: 15, remaining: 15)

# Beto
beto = User.create(name: 'Beto Antônio Neto da Silva', email: 'beto@gmail.com', username: 'betoans')
pb = Portfolio.create(title: 'Carteira do Beto', user_id: beto.id)
Stock.create(portfolio_id: pb.id, symbol: 'PETR4', purchase_price: 89.00, quantity: 50, remaining: 50)

# Gertrudes
gertrudes = User.create(name: 'Gertrudes Lindalva Pereira', email: 'gertrudes@gmail.com', username: 'gertrudeslp')
pg = Portfolio.create(title: 'Carteira da Gertrudes', user_id: gertrudes.id)
Stock.create(portfolio_id: pg.id, symbol: 'MGLU3', purchase_price: 100.00, quantity: 20, remaining: 20)

# João
joao = User.create(name: 'João Reinaldo Abílio Antunes', email: 'joao@gmail.com', username: 'joaoraa')
pj1 = Portfolio.create(title: 'Carteira do João', user_id: joao.id)
pj2 = Portfolio.create(title: 'Segunda carteira do João', user_id: joao.id)
Stock.create(portfolio_id: pj1.id, symbol: 'BBAS3', purchase_price: 59.00, quantity: 70, remaining: 70)
Stock.create(portfolio_id: pj2.id, symbol: 'BBDC4', purchase_price: 290.00, quantity: 3, remaining: 3)

# rails admin code
AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password') if Rails.env.development?